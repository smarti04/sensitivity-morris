'''
==============
script qui permet de lancer une analyse de sensibilité de Morris très simple
==============

@smartinez
'''
# coding: utf-8
# --------------------------------------------------------------------------------
#                                Packages
# --------------------------------------------------------------------------------
import inloop
import numpy as np
# plan d'experience de Morris
from SALib.sample.morris import sample
import pandas as pd
from multiprocessing import Pool
import os
# --------------------------------------------------------------------------------
#                                problem
# --------------------------------------------------------------------------------
problem = {
    'num_vars': 2,
    'names': ['batman', 'robin'],
    'bounds': [
        [0, 1],      # 0  - batman
        [0, 1]        # 1  - robin
    ]
        }
# --------------------------------------------------------------------------------
#                          Plan d'experiences - laborieux
# --------------------------------------------------------------------------------
#Morris
traj = 2
param_values = sample(problem, traj, num_levels=4)
# --------------------------------------------------------------------------------
#                   Ajout du numero de ligne (laborieux)
# --------------------------------------------------------------------------------
listeParam = []
for paramVAl, numVal in zip(param_values, range(len(param_values))):
    listeParam.append(np.append(paramVAl, numVal))
listeParam = np.asarray(listeParam, dtype=float)
# --------------------------------------------------------------------------------
#                          Gestion des paths
# --------------------------------------------------------------------------------
path_sim = 'E:/test-morris/'
def f(param):
    nbr = str(int(param[len(param) - 1]))
    x = nbr + '_'+'batman{}_robin{}'.format(round(param[0], 2),
                                round(param[1], 2))
    # --------------------------------------------------------------------------------
    #                  Sauvegarde des inputs dans un fichier  .csv
    # --------------------------------------------------------------------------------
    df = pd.DataFrame({
        'val': param[:-1]
    },
        index=problem['names']).transpose()
    df.to_csv(path_sim + '/'+ nbr + '_data.csv')
    # --------------------------------------------------------------------------------
    #                         Lancement du RUN
    # --------------------------------------------------------------------------------
    inloop.run( x1 = param[0],
                x2 = param[1],
                name =  x,
                path_sim = path_sim)
# --------------------------------------------------------------------------------
#                             Exécution du code
# --------------------------------------------------------------------------------
def pool_handler():
    p = Pool(6)
    p.map(f, listeParam)

if __name__ == "__main__":  # confirms that the code is under main function
    pool_handler()
